﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MapApplication
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        /* async function wait for the interaction user & send to an antoher page */

        /*Page Functionnal Map */
        async void OnNextPage1ButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }
        /*Page Street Map */
        async void OnNextPage2ButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }
        /*Page Satellite Map */
        async void OnNextPage3ButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }
    }
}
