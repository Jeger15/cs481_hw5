﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;

/*Documentation -> docs.microsoft.com/en-us/dotnet/api/xamarin.forms.picker?view=xamarin-forms */

namespace MapApplication
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();

			/* Initialization of Map & Position */
			var map3 = new Map(
                MapSpan.FromCenterAndRadius(new Position(48.8566969, 2.3514616), Distance.FromMiles(10)))
            {
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            map3.MapType = MapType.Satellite;
            var stack = new StackLayout { Spacing = 0 };
            stack.Children.Add(map3);
            Content = stack;

			/* Initialization of Pin & Position */
			var position = new Position(48.9215226, 2.2192606); // Latitude, Longitude
			var position2 = new Position(48.83591094326953, 2.2397744166198708); // Latitude, Longitude
			var position3 = new Position(48.84615436557261, 2.3382079670670963); // Latitude, Longitude
			var position4 = new Position(48.84675683897101, 2.2791779529768563); // Latitude, Longitude
			var pin1 = new Pin
			{
				Type = PinType.Place,
				Position = position,
				Label = "Best Sandwich in Paris !"
			};
			var pin2 = new Pin
			{
				Type = PinType.Place,
				Position = position2,
				Label = "Best Food in Paris !"
			};
			var pin3 = new Pin
			{
				Type = PinType.Place,
				Position = position3,
				Label = "Best Place for cheap Shopping !"
			};
			var pin4 = new Pin
			{
				Type = PinType.Place,
				Position = position4,
				Label = "Best betting docks to land between friends !"
			};

			map3.Pins.Add(pin1);
			map3.Pins.Add(pin2);
			map3.Pins.Add(pin3);
			map3.Pins.Add(pin4);

			/* Picker For Choose the Place & Go on the Postion*/
			Dictionary<string, Position> TabForPin = new Dictionary<string, Position>
		{
			{ "Alcatraz", pin1.Position }, { "Le Délice", pin2.Position },
			{ "Les Passages", pin3.Position }, { "Quais de St Michel", pin4.Position }
		};


			Picker pickerPlace = new Picker
			{
				Title = "           Choose The Best Place in Paris !",
				HeightRequest = 100
			};

			foreach (string pinName in TabForPin.Keys)
				pickerPlace.Items.Add(pinName);

			pickerPlace.SelectedIndexChanged += (sender, args) =>
			{
				if (pickerPlace.SelectedIndex == -1) {
					Position pos = new Position(48.862725, 2.287592);
					map3.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(pos.Latitude, pos.Longitude), Distance.FromKilometers(10)));
				}
				else {
					string pinName = pickerPlace.Items[pickerPlace.SelectedIndex];
					map3.MoveToRegion(MapSpan.FromCenterAndRadius(TabForPin[pinName], Distance.FromKilometers(1)));
				}
			};

			stack.Children.Add(pickerPlace);
			Content = stack;
		}
	}
}